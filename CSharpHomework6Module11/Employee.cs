﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpHomework6Module11
{
    public struct Employee
    {
        public string Name { private set; get; }
        public string Sername { private set; get; }
        public double Salary { private set; get; }
        public string Position { private set; get; }
        public DateTime Date;


        public Employee(string name, string sername, double salary, string position, int year, int month, int day)
        {
            Name = name;
            Sername = sername;
            Salary = salary;
            Position = position;
            Date = new DateTime(year, month, day);

        }

        public void AllInfo()
        {
            Console.WriteLine(Name + " " + Sername);
            Console.WriteLine("Salary is " + (double)Salary);
            Console.WriteLine("Position is " + Position);
            Console.WriteLine("Date of work start is " + Date);
            Console.WriteLine();
        }
    }
}
