﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpHomework6Module11
{
    public struct Student
    {

        public string Name { private set; get; }
        public double AvgScore { private set; get; }
        public double Income1 { private set; get; }
        public double Income2 { private set; get; }
        public int Gender { private set; get; }
        public int EducationForm { private set; get; }
        public double Average { private set; get; }


        public Student(string name, double avgScore, double income1, int gender, int educationForm, double income2 = 0)
        {
            Name = name;
            AvgScore = avgScore;
            Income1 = income1;
            Income2 = income2;
            Gender = gender;
            EducationForm = educationForm;
            Average = (income2 + income1) / 2;
        }
    }
}
