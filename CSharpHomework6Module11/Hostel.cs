﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpHomework6Module11
{
    public struct Hostel
    {
        public List<Student> QueueHostel(List<Student> students)
        {
            var hostelList = students.OrderBy(i => i.Name).Where(i => (i.Average) / 2 < 800).ToList();
            hostelList.AddRange(students.OrderByDescending(i => i.AvgScore).Where(i => (i.Average) / 2 > 800));

            return hostelList;
        }
    }
}
