﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpHomework6Module11
{
    class Program
    {
        enum GenderEnum
        {
            male = 1,
            female
        }
        enum EducationFormEnum
        {
            morning = 1,
            dinner,
            evening
        }

        static void Main(string[] args)
        {
            MethodsOffice methods = new MethodsOffice();
            //---------------------------------------------------------------------------
            //task1
            Console.WriteLine("************************************************************************");
            Console.WriteLine("task1");
            Console.WriteLine();

            Office office = new Office()
            {
                employeer = new Employee[]
                {
                new Employee("John", "Doe", 10000, "Boss", 2014, 12, 8 ),
                new Employee("John", "Wick", 1200, "Manager", 2015, 12, 16),
                new Employee("Jessie", "Quick", 1000, "Manager", 2016, 10, 18),
                new Employee("Bruce", "Wayne", 980, "Manager", 2017, 11, 25),
                new Employee("Barry", "Allen", 500, "Clerk", 2014, 9, 30),
                new Employee("Walley", "West", 350, "Clerk", 2013, 5, 13),
                new Employee("Black", "Thunder", 450, "Clerk", 2016, 5, 17),
                new Employee("Lord", "Dark", 400, "Clerk", 2018, 3, 19),
                new Employee("Roi", "Harper", 450, "Clerk", 2012, 6, 5),
                new Employee("Jimm", "Earthworm", 500, "Clerk", 2011, 8, 29),
                }
            };

            foreach (var item in office.employeer)
            {
                item.AllInfo();
            }


            Console.WriteLine("---------------------------------------------------------------------");
            Console.WriteLine("Best managers is: ");
            foreach (var item in methods.BestManagers(office))
            {
                item.AllInfo();
            }

            Console.WriteLine("---------------------------------------------------------------------");
            Console.WriteLine("Sort by date: ");
            foreach (var item in methods.DateSort(office))
            {
                item.AllInfo();
            }

            //---------------------------------------------------------------------------
            //task2
            Console.WriteLine();
            Console.WriteLine("************************************************************************");
            Console.WriteLine("task2");
            Console.WriteLine();

            var hostelList = new List<Student>()
            {
                new Student("John Wick", 98, 1000, (int)GenderEnum.male, (int)EducationFormEnum.morning,1000),
                new Student ("Jessie Quick", 80, 2500, (int)GenderEnum.female, (int)EducationFormEnum.dinner, 500 ),
                new Student ("Bruce Wayne", 100, 1000000, (int)GenderEnum.male, (int)EducationFormEnum.evening ),
                new Student ("Barry Allen", 95, 1500, (int)GenderEnum.male, (int)EducationFormEnum.morning,750 ),
                new Student ("Clark Kent", 85, 1000, (int)GenderEnum.male, (int)EducationFormEnum.evening ),
                new Student ("Hal Jordan", 75, 1800, (int)GenderEnum.male, (int)EducationFormEnum.evening, 900),
                new Student ("Ray Palmer", 100, 3000, (int)GenderEnum.male, (int)EducationFormEnum.morning, 1500),
                new Student ("Zatanna Zatara", 60, 1500, (int)GenderEnum.female, (int)EducationFormEnum.dinner),
                new Student ("Oliver Queen", 70, 900000, (int)GenderEnum.male, (int)EducationFormEnum.morning)

            };

            Hostel hostel = new Hostel();

            foreach (var item in hostel.QueueHostel(hostelList))
            {
                Console.WriteLine(item.Name);
            }



            Console.ReadLine();
        }
    }
}
