﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpHomework6Module11
{
    public struct Office : IEnumerable
    {


        public Employee[] employeer;

        public Office(int size)
        {
            employeer = new Employee[size];
        }

        public Employee this[int index]
        {
            get
            {
                return employeer[index];
            }
            set
            {
                employeer[index] = value;
            }
        }

        public IEnumerator GetEnumerator()
        {
            return employeer.GetEnumerator();
        }
    }
}



