﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpHomework6Module11
{
    public class MethodsOffice
    {
        public Employee[] BestManagers(Office office)
        {
            double result = office.employeer.Where(i => i.Position == "Clerk").Average(i => i.Salary);
            Employee[] bestManagers = office.employeer.Where(i => i.Position == "Manager").Where(i => i.Salary > result).ToArray();
            return bestManagers;
        }

        public Employee[] DateSort(Office office)
        {
            DateTime tDate;
            Employee[] datesort = office.employeer.OrderBy(i => i.Name).Where(i => i.Date < (tDate = new DateTime(2014, 12, 8))).ToArray();
            return datesort;
        }
    }
}
